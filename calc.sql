DROP TABLE IF EXISTS citreq;
CREATE TABLE citreq(
  id INTEGER NOT NULL PRIMARY KEY,
  population INTEGER NOT NULL,
  lat REAL NOT NULL,
  lon REAL NOT NULL,
  rlat REAL NOT NULL,
  rlon REAL NOT NULL
);

INSERT INTO citreq
SELECT
  id,
  population,
  lat,
  lon,
  -lat AS rlat,
  CASE WHEN lon < 0 THEN lon + 180 ELSE lon - 180 END AS rlon
FROM cities;

DROP TABLE IF EXISTS dist;
CREATE TABLE dist(
  id1 INTEGER NOT NULL,
  id2 INTEGER NOT NULL,
  rdist REAL NOT NULL,
  PRIMARY KEY(id1, id2)
);

INSERT INTO dist
SELECT
  a.id AS id1,
  b.id AS id2,
  ACOS(
    SIN(a.lat*PI()/180)*SIN(b.lat*PI()/180) + COS(a.lat*PI()/180)*COS(b.lat*PI()/180)*COS(b.lon*PI()/180-a.lon*PI()/180)
  ) * 6370 AS dist_km
FROM citreq a, citreq b
WHERE
  a.id < b.id
  AND
  a.population > 262144
  AND
  b.population > 262144
