const sqlite3 = require('sqlite3').verbose();

let dat = require('./geonames-all-cities-with-a-population-1000.json');

console.log(dat.length);

let db = new sqlite3.Database('./cities.db');

let n = 0;

for (let city of dat) {
  n++;
  if (!(n%16384)) {
    console.log(city);
  }
  db.run(`INSERT INTO cities(id, lon, lat, population, name) VALUES(?,?,?,?,?)`, [
    n, city.coordinates.lon, city.coordinates.lat, city.population, city.ascii_name]);
}

db.close();
